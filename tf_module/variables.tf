variable "environment" {
  type        = string
  default     = "dev"
  description = "The environment to deploy to"
}

variable "service" {
  type        = string
  description = "Name of the service"
}
