#!/usr/bin/env python3
import csv
import argparse

TEMPLATE = '''
host {name} {{
   option host-name "{name}";
   hardware ethernet {mac};
   fixed-address {ip};
}}
'''

def get_parser():
    parser = argparse.ArgumentParser(description='Process task 5')
    parser.add_argument('input_file')
    parser.add_argument('output_file')
    return parser


def main():
    parser = get_parser()
    args = parser.parse_args()
    with open(args.input_file, 'r') as f:
        reader = csv.reader(f)
        with open(args.output_file, 'a') as out:
            for row in reader:
                if not row[0].startswith('127'):
                    continue
                out.write(TEMPLATE.format(ip=row[0], mac=row[1], name=row[2]))

if __name__ == '__main__':
    main()
