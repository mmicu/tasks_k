FROM ubuntu:21.10
# Inspired from https://github.com/uphold/docker-litecoin-core/blob/master/0.18/Dockerfile

ENV LITECOIN_VERSION=0.18.1
ENV LITECOIN_DATA=/home/litecoin/.litecoin

RUN adduser litecoin
RUN apt-get update -y \
  && apt-get install -y curl gnupg

# Add the public key that signs the build
# https://stackoverflow.com/questions/25074877/cant-check-signature-public-key-not-found
# This follows the same idea form https://github.com/uphold/docker-litecoin-core/blob/master/0.18/Dockerfile#L17
# to search in multiple keyservers as some of them may be down (this happened during testing in CI)
RUN gpg --keyserver pgp.mit.edu  --receive-keys 59CAF0E96F23F53747945FD4FE3348877809386C || \
    gpg --keyserver keyserver.pgp.com --receive-keys 59CAF0E96F23F53747945FD4FE3348877809386C || \
    gpg --keyserver ha.pool.sks-keyservers.net --receive-keys 59CAF0E96F23F53747945FD4FE3348877809386C || \
    gpg --keyserver keys.gnupg.net --receive-keys 59CAF0E96F23F53747945FD4FE3348877809386C

# Get the project and the signature
RUN curl -SLO https://download.litecoin.org/litecoin-${LITECOIN_VERSION}/linux/litecoin-${LITECOIN_VERSION}-x86_64-linux-gnu.tar.gz \
  && curl -SLO https://download.litecoin.org/litecoin-${LITECOIN_VERSION}/linux/litecoin-${LITECOIN_VERSION}-linux-signatures.asc


# Check if the signature was produced by a trusted key
RUN gpg --verify litecoin-${LITECOIN_VERSION}-linux-signatures.asc

# Check if the check sum of build was included in included in the signature (basically check if this is the build that was signed)
RUN grep $(sha256sum litecoin-${LITECOIN_VERSION}-x86_64-linux-gnu.tar.gz | awk '{ print $1 }') litecoin-${LITECOIN_VERSION}-linux-signatures.asc

# Unarchived the file and remove the archive
RUN tar --strip=2  -xzf *.tar.gz -C /usr/local/bin \
  && rm *.tar.gz

# Get the entry point
COPY entrypoint.sh /entrypoint.sh
RUN chown litecoin:litecoin /entrypoint.sh \
  && chmod u+x /entrypoint.sh

# Switch the user
USER litecoin
# Create the data directory
RUN mkdir -p /home/litecoin/.litecoin
# Mark the data directory as being an external volume
VOLUME ["/home/litecoin/.litecoin"]

# Set the entry point for the container
ENTRYPOINT ["/entrypoint.sh"]

# By default run the daemon
CMD ["-datadir=/home/litecoin/.litecoin"]
