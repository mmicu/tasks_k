locals {
  # limit the size of the variables as some AWS resource have limits on the name size
  environment = substr(lower(var.environment), 0, 4)
  service     = substr(lower(var.service), 0, 10)
  suffix      = format("%s-%s", local.environment, local.service)
}

provider "aws" {}

data "aws_caller_identity" "source" {}

# Main resources
################################################################################

resource "aws_iam_role" "role" {
  name               = format("%s-role", local.suffix)
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy.json
}

resource "aws_iam_group" "group" {
  name = format("%s-group", local.suffix)
}

resource "aws_iam_group_policy" "policy" {
  name   = format("%s-policy", local.suffix)
  group  = aws_iam_group.group.name
  policy = data.aws_iam_policy_document.policy.json
}

resource "aws_iam_user" "user" {
  name = format("%s-user", local.suffix)
}

resource "aws_iam_user_group_membership" "membership" {
  user = aws_iam_user.user.name

  groups = [
    aws_iam_group.group.name,
  ]
}

# Auxiliary resources
################################################################################

data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.source.account_id}:root"]

    }
  }
}

data "aws_iam_policy_document" "policy" {
  statement {
    effect    = "Allow"
    actions   = ["sts:AssumeRole"]
    resources = [aws_iam_role.role.arn]
  }
}

