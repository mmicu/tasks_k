#!/usr/bin/env bash


if [[ $# -lt 2 ]]; then
  echo "The script requires two arguments."
  echo "Usage: $0 <input_file> <output_file>"
  exit 1
fi


for line in $(cat $1 | grep "^127" ); do
  IP="$(echo $line | cut -d, -f1)"
  MAC="$(echo $line | cut -d, -f2)"
  NAME="$(echo $line | cut -d, -f3)"
  echo "IP: $IP"
  echo "MAC: $MAC"
  echo "NAME: $NAME"
  cat <<- EOF >> $2
host $NAME {
   option host-name "$NAME";
   hardware ethernet $MAC;
   fixed-address $IP;
}
EOF
done
