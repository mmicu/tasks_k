## Task 1

Docker-ayes:
Write a Dockerfile to run Litecoin 0.18.1 in a container. It should somehow verify the
checksum of the downloaded release (there's no need to build the project), run as a normal user, and
when run without any modifiers (i.e. docker run somerepo/litecoin:0.18.1) should run the daemon, and
print its output to the console. The build should be security conscious (and ideally pass a container
image security test such as Anchore).


### Solution


Here is the [Dockerfile](./Dockerfile)

For vulnerability scans, I don't have access to https://anchore.com but, I used the following:

* https://github.com/anchore/grype - 0 vulnerability
* https://docs.docker.com/engine/scan/ - 13 low vulnerability
* https://github.com/aquasecurity/trivy - Total: 0 (UNKNOWN: 0, LOW: 0, MEDIUM: 0, HIGH: 0, CRITICAL: 0)


I got inspired by the following links:

* https://github.com/uphold/docker-litecoin-core
* https://stackoverflow.com/questions/25074877/cant-check-signature-public-key-not-found
* https://www.linuxbabe.com/security/verify-pgp-signature-software-downloads-linux


## Task 2

k8s FTW: Write a Kubernetes StatefulSet to run the above, using persistent volume claims and
resource limits.


### Solution

Under the [manifests/](./manifests) we have the YAML definitions.

[stateful.yaml](./manifests/statefulset.yaml) is the StatefulSet that uses a `volumeClaimTemplates` to request a persistent volume.
I omitted the `storageClass` to use the default one. If this would run on AWS (using EBS volumes) we would also need to make sure each pod is scheduled in the same AZ, because EBS volumes can't be mounted across zones.

[pdb.yaml](./manifests/pdb.yaml) was added extra metadata for about the desired availability of the service. In this case, we want only one pod to be unavailable at a time. Tools that drain nodes should follow this restriction.

## Task 3

All the continuous: Write a simple build and deployment pipeline for the above using groovy /
Jenkinsfile, Travis CI or Gitlab CI.

### Solution

I used GitLab because I can test it easily online and I have more experience with it.
The solution is at [.gitlab-ci.yml](./.gitlab-ci.yml)

I used multiple resources:

* GitLab docs on how to build a docker image using kaniko - https://gitlab.cern.ch/gitlabci-examples/build_docker_image/-/blob/master/.gitlab-ci.yml
* Customize example for building the manifests - https://github.com/kubernetes-sigs/kustomize

Some improvement points:

* Addend review environments would be ideal (so each MR can test the changes)
* Integrate using GitLab Environment concept
* Run actual tests and security checks before deploy
* We are using public images for kustomize and kaniko. Ideally we would create our own based on the public images, it requires a bit more maintenance but has some interesting advantages :
  * We can add extra tools in them
  * We can shape them how for our usecase (for example, I need to specify `entripoint: [""]` to make thins work)
  * We minimize supply chain attacks
  * We also rely less on random infrastructure (for example, docker registries)

## Task 4

Given a CVS file with the following format:  `IP, MAC address, computer name`

Here is an example file (can also be found also in [data.csv](./data.csv)):
```
127.b.c.d,AA:BB:CC:DD:EE:FF,computer_a
10.b.c.d,AA:BB:CC:DD:EE:FF,computer_b
127.x.x.x,AA:BB:CC:DD:EE:FF,computer_c
11.x.x.x,AA:BB:CC:DD:EE:FF,computer_d
```

Create a script that will take two arguments  `./script.sh input_file_path output_file_path`
that for each line in the CSV will generate in the `output_file_path` an entry like if the IP starts with 127.:
```
host computer_x {
   option host-name "computer_a";
   hardware ethernet AA:BB:CC:DD:EE:FF;
   fixed-address 127.b.c.d;
}
```

### Solution

The solution was done using bash [here](./task_4.py)

## Task 5

The same question as [Task 4](task-4).

### Solution

The solution was done using python [here](./task_5.py)

## Task 6

 Terraform lovers unite: write a Terraform module that creates the following resources in IAM:

* A role, with no permissions, which can be assumed by users within the same account,
* A policy, allowing users / entities to assume the above role,
* A group, with the above policy attached,
* A user, belonging to the above group.

All four entities should have the same name, or be similarly named in some meaningful way given the
context e.g. prod-ci-role, prod-ci-policy, prod-ci-group, prod-ci-user; or just prod-ci. Make the suffixes
toggleable, if you like.

### Solution

The TF module can be found [here](./tf_module)
